django-hgrepo
-------------

A Django application for managing and providing access control for Mercurial
repositories.

Installation
------------

This app depends on the Python module `hglib`. If you're using pip:

    pip install python-hglib

This app is meant to run in conjunction with other applications, though can be
used on its own. Consult the documentation of the client application, though
normally installation will consist of copying/softlinking the `hgrepo`
directory into the project directory.

Configuration
-------------

There are a few configuration options for `settings.py`.

    :::python
    ## The root path for the repositories
    HGREPO_ROOT = '/var/lib/django-hgrepo'

    ## A list of IP addresses which are allowed to query the web server for
    ## access control information.
    HGREPO_ACCESS_ACL = ['127.0.0.1', '::1', '::ffff:127.0.0.1']

    ## A function which can override the default behavior for deciding access
    ## the result of an access. See "Custom Access Rules". If None, it uses
    ## the default behavior.
    HGREPO_ACCESS_FUNCTION = None

This app attempts to adopt the "single-user-many-keys" model which is popular
in various VCS services, although it does not deal with public keys or any type
of authentication directly. You should use [django-sshkey][1] to help map
public keys to users, or use your own solution.

Dispatch Script
---------------

The dispatch script at the root is meant to query for access rules over HTTP
for performance reasons. The usage of the script is like so:

    ./django-hgrepo-ssh <url> [<username>]

where `<url>` is the URL where the `hgrepo.views.access` view has been mounted
via the URLconf (probably something like `http://example.com/hgrepo/access`),
and `<username>` is the username for the user we are querying for. The dispatch
script is useful as the `command` option to an `authorized_keys` entry. If you
are using [django-sshkey][1], you should set this script as your
`SSHKEY_AUTHORIZED_KEYS_COMMAND`:

    :::python
    SSHKEY_AUTHORIZED_KEYS_COMMAND='/path/to/django-hgrepo-ssh http://example.com/hgrepo/access {username}'

Custom Access Rules
-------------------

The default access control behavior is to grant read/write where an `HgAccess`
object exists with its `write` flag set for a given repository and user. If an
object exists with no write flag, grant read access. If a repository is public,
grant read access. Otherwise, grant no access. See `hgrepo.views.model_access`
for the implementation.

You can fill in any arbitrary access logic you want by setting
`HGREPO_ACCESS_FUNCTION` to a function which accepts an `HgRepo` and an
optional `User` object to determine access. It should return `'rw'` for
read/write access, `'r'` for read access, or `None` for no access. Here is an
example which does simple username checking to determine access:

    :::python
    def HGREPO_ACCESS_FUNCTION(repo, user):
      if user is not None:
        ## Fred has read/write access to all repositories
        if user.username == 'fred':
          return 'rw'
        
        ## Sally has read access to all repositories
        if user.username == 'sally':
          return 'r'
          
      ## Public repos are readable by all 
      if repo.public:
        return 'r'

      return None

[1]: https://bitbucket.org/ClemsonSoCUnix/django-sshkey "django-sshkey"
