import os
from setuptools import setup, find_packages

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

# fetch README contents
README = open('README.md').read()

# generate package names
hgrepo_dir = 'hgrepo'
packages = [hgrepo_dir] + [hgrepo_dir+'.'+x
                           for x in find_packages(hgrepo_dir)]

# read requirements from file
def reqs(*f):
  path = os.path.join(*f)
  out = []
  with open(path) as fp:
    for l in fp:
      # strip out comments
      l = l.split('#', 1)[0].strip()
      if l:
        out.append(l)
  return out

setup(
  name='django-hgrepo',
  packages=packages,
  description='A Django application for managing Mercurial repositories',
  long_description=README,
  url='https://bitbucket.org/ClemsonSoCUnix/django-hgrepo',
  author='Clemson School of Computing Unix Admins',
  # FIXME: author_email='socunix_developers@lists.clemson.edu',
  classifiers=[
    'Environment :: Web Environment',
    'Framework :: Django',
    'Intended Audience :: System Administrators',
    'Operating System :: POSIX',
    'Programming Language :: Python',
    'Programming Language :: Python :: 2.7',
    'Topic :: Internet :: WWW/HTTP',
    'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
  ],
  scripts=[
    'django-hgrepo-ssh',
  ],
  install_requires=[
    reqs('requirements.txt'),
  ],
)
