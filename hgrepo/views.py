from django.http import HttpResponse, HttpResponseNotFound
from django.core.exceptions import PermissionDenied
from django.contrib.auth.models import User
from hgrepo.models import HgRepo, HgAccess
from hgrepo import settings
import json

def access(request, repo):
  remote_addr = request.META.get('REMOTE_ADDR', '')
  if remote_addr not in settings.HGREPO_ACCESS_ACL:
    raise PermissionDenied

  try:
    repo = HgRepo.objects.get(name=repo)
  except HgRepo.DoesNotExist:
    message = 'Repository does not exist: %s\n' % repo
    return HttpResponseNotFound(message, content_type='text/plain')

  try:
    username = request.GET['u']
    user = User.objects.get(username=username)
  except KeyError:
    user = None

  access = (settings.HGREPO_ACCESS_FUNCTION or model_access)(repo, user)

  if access is None:
    data = {'access':'none'}
  else:
    data = { 'access': access, 'path': repo.abspath }
  return HttpResponse(json.dumps(data), content_type='application/json')

def model_access(repo, user):
  if user is not None:
    try:
      hgaccess = HgAccess.objects.get(user=user, repo=repo)
      if hgaccess.write:
        return 'rw'
      else:
        return 'r'
    except HgAccess.DoesNotExist:
      pass

  if repo.public:
    return 'r'

  return None
