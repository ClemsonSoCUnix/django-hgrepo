from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.contrib.auth.models import User
from hgrepo import settings
import hglib
import os
import shutil

class HgRepo(models.Model):
  name = models.CharField(max_length=100, unique=True, db_index=True)
  path = models.CharField(max_length=255, unique=True)
  public = models.BooleanField(default=False)

  class Meta:
    verbose_name = 'Mercurial Repository'
    verbose_name_plural = 'Mercurial Repositories'

  def __unicode__(self):
    return self.name

  def __init__(self, *args, **kwargs):
    super(HgRepo, self).__init__(*args, **kwargs)
    self.abspath = os.path.join(settings.HGREPO_ROOT, self.path)

  def __del__(self):
    try:
      self._repo.close()
    except AttributeError:
      pass

  @property
  def repo(self):
    try:
      return self._repo
    except AttributeError:
      self._repo = hglib.open(self.abspath)
      return self._repo

  def post_save(self, created, **kwargs):
    if created:
      hglib.init(self.abspath)

  def post_delete(self, **kwargs):
    shutil.rmtree(self.abspath)
    d = os.path.dirname(self.abspath)
    while d != settings.HGREPO_ROOT:
      try:
        os.rmdir(d)
        d = os.path.dirname(d)
      except OSError, e:
        import errno
        if e.errno != errno.ENOTEMPTY:
          raise
        break

class HgAccess(models.Model):
  user = models.ForeignKey(User, db_index=True)
  repo = models.ForeignKey(HgRepo, db_index=True)
  write = models.BooleanField(default=False)

  class Meta:
    unique_together = ('user', 'repo')
    verbose_name = 'Access Rights'
    verbose_name_plural = 'Access Rights'

  def __unicode__(self):
    return u'%s/%s' % (self.user, self.repo)

@receiver(post_save, sender=HgRepo, dispatch_uid=__name__+'.post_save_hgrepo')
def post_save_hgrepo(sender, instance, **kwargs):
  instance.post_save(**kwargs)

@receiver(post_delete, sender=HgRepo, dispatch_uid=__name__+'.post_delete_hgrepo')
def post_delete_hgrepo(sender, instance, **kwargs):
  instance.post_delete(**kwargs)
