from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from hgrepo.models import HgRepo, HgAccess
from hgrepo import settings
import hglib
import json
import os.path

class HgRepoTests(TestCase):
  def setUp(self):
    from tempfile import mkdtemp
    self.original_root = settings.HGREPO_ROOT
    settings.HGREPO_ROOT = mkdtemp(prefix='hgrepo-test')

  def tearDown(self):
    from shutil import rmtree
    HgRepo.objects.all().delete()
    rmtree(settings.HGREPO_ROOT)
    settings.HGREPO_ROOT = self.original_root

  def test_create(self):
    hgrepo = HgRepo.objects.create(name='a', path='a')
    self.assertEqual(os.path.join(settings.HGREPO_ROOT, 'a'), hgrepo.abspath)
    self.assertTrue(os.path.isdir(hgrepo.abspath))

  def test_delete(self):
    hgrepo = HgRepo.objects.create(name='a', path='a')
    abspath = hgrepo.abspath
    hgrepo.delete()
    self.assertFalse(os.path.exists(abspath))

  def test_create_name_ne_path(self):
    hgrepo = HgRepo.objects.create(name='a', path='b')
    self.assertEqual(os.path.join(settings.HGREPO_ROOT, 'b'), hgrepo.abspath)
    self.assertTrue(os.path.isdir(hgrepo.abspath))

  def test_create_path_subdir(self):
    hgrepo = HgRepo.objects.create(name='a', path='a/b')
    self.assertEqual(os.path.join(settings.HGREPO_ROOT, 'a/b'), hgrepo.abspath)
    self.assertTrue(os.path.isdir(hgrepo.abspath))

  def test_delete_path_subdir(self):
    hgrepo = HgRepo.objects.create(name='a', path='a/b')
    abspath = hgrepo.abspath
    hgrepo.delete()
    self.assertFalse(os.path.isdir(hgrepo.abspath))
    self.assertFalse(os.path.isdir(os.path.dirname(hgrepo.abspath)))

  def test_repo_is_hgclient(self):
    hgrepo = HgRepo.objects.create(name='a', path='a')
    self.assertIsInstance(hgrepo.repo, hglib.client.hgclient)

  def test_nonexist_anonymous_access(self):
    client = Client()
    url = reverse('access', kwargs={'repo':'a'})
    response = client.get(url)
    self.assertEqual(response.status_code, 404)

  def test_private_anonymous_access(self):
    hgrepo = HgRepo.objects.create(name='a', path='a', public=False)
    client = Client()
    url = reverse('access', kwargs={'repo':'a'})
    response = client.get(url)
    self.assertEqual(response.status_code, 200)
    self.assertIn('Content-Type', response)
    self.assertEqual(response['Content-Type'], 'application/json')
    data = json.loads(response.content)
    self.assertIn('access', data)
    self.assertEqual(data['access'], 'none')
    self.assertNotIn('path', data)

  def test_private_user_access(self):
    user = User.objects.create(username='user', email='user@example.com')
    hgrepo = HgRepo.objects.create(name='a', path='a', public=False)
    client = Client()
    url = reverse('access', kwargs={'repo':'a'})
    response = client.get(url, {'u':'user'})
    self.assertEqual(response.status_code, 200)
    self.assertIn('Content-Type', response)
    self.assertEqual(response['Content-Type'], 'application/json')
    data = json.loads(response.content)
    self.assertIn('access', data)
    self.assertEqual(data['access'], 'none')
    self.assertNotIn('path', data)

  def test_public_anonymous_access(self):
    hgrepo = HgRepo.objects.create(name='a', path='a', public=True)
    client = Client()
    url = reverse('access', kwargs={'repo':'a'})
    response = client.get(url)
    self.assertEqual(response.status_code, 200)
    self.assertIn('Content-Type', response)
    self.assertEqual(response['Content-Type'], 'application/json')
    data = json.loads(response.content)
    self.assertIn('access', data)
    self.assertEqual(data['access'], 'r')
    self.assertIn('path', data)
    self.assertEqual(data['path'], hgrepo.abspath)

  def test_public_user_access(self):
    user = User.objects.create(username='user', email='user@example.com')
    hgrepo = HgRepo.objects.create(name='a', path='a', public=True)
    client = Client()
    url = reverse('access', kwargs={'repo':'a'})
    response = client.get(url, {'u':'user'})
    self.assertEqual(response.status_code, 200)
    self.assertIn('Content-Type', response)
    self.assertEqual(response['Content-Type'], 'application/json')
    data = json.loads(response.content)
    self.assertIn('access', data)
    self.assertEqual(data['access'], 'r')
    self.assertIn('path', data)
    self.assertEqual(data['path'], hgrepo.abspath)

  def test_read_access(self):
    user = User.objects.create(username='user', email='user@example.com')
    hgrepo = HgRepo.objects.create(name='a', path='a', public=False)
    hgaccess = HgAccess.objects.create(user=user, repo=hgrepo, write=False)
    client = Client()
    url = reverse('access', kwargs={'repo':'a'})
    response = client.get(url, {'u':'user'})
    self.assertEqual(response.status_code, 200)
    self.assertIn('Content-Type', response)
    self.assertEqual(response['Content-Type'], 'application/json')
    data = json.loads(response.content)
    self.assertIn('access', data)
    self.assertEqual(data['access'], 'r')
    self.assertIn('path', data)
    self.assertEqual(data['path'], hgrepo.abspath)

  def test_write_access(self):
    user = User.objects.create(username='user', email='user@example.com')
    hgrepo = HgRepo.objects.create(name='a', path='a', public=False)
    hgaccess = HgAccess.objects.create(user=user, repo=hgrepo, write=True)
    client = Client()
    url = reverse('access', kwargs={'repo':'a'})
    response = client.get(url, {'u':'user'})
    self.assertEqual(response.status_code, 200)
    self.assertIn('Content-Type', response)
    self.assertEqual(response['Content-Type'], 'application/json')
    data = json.loads(response.content)
    self.assertIn('access', data)
    self.assertEqual(data['access'], 'rw')
    self.assertIn('path', data)
    self.assertEqual(data['path'], hgrepo.abspath)
