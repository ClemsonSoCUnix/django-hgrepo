from django.contrib import admin
from hgrepo.models import HgRepo, HgAccess

class HgRepoAdmin(admin.ModelAdmin):
  list_display = ['name']
  search_fields = ['name']

class HgAccessAdmin(admin.ModelAdmin):
  list_display = ['repo', 'user']
  search_fields = ['repo__name', 'user__username']

admin.site.register(HgRepo, HgRepoAdmin)
admin.site.register(HgAccess, HgAccessAdmin)
