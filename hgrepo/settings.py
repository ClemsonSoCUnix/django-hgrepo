from django.conf import settings

HGREPO_ROOT = getattr(settings, 'HGREPO_ROOT', '/var/lib/django-hgrepo')
HGREPO_ACCESS_ACL = getattr(settings, 'HGREPO_ACCESS_ACL', ['127.0.0.1', '::1', '::ffff:127.0.0.1'])
HGREPO_ACCESS_FUNCTION = getattr(settings, 'HGREPO_ACCESS_FUNCTION', None)
