import json
import mercurial.dispatch
import os
import sys
import urllib

def dispatch(access_url, repo_name, username):
  url = '%s/%s' % (access_url, repo_name)
  if username is not None:
    url += '?u=' + urllib.quote_plus(username)
    os.environ['HGREPO_USER'] = username
  response = urllib.urlopen(url)
  code = response.getcode()
  mime = response.info().gettype()
  if code != 200:
    sys.stderr.write('Backend returned code %s\n' % code)
    if mime == 'text/plain':
      sys.stderr.write(response.read())
    sys.exit(1)
  if mime != 'application/json':
    sys.stderr.write('Backend returned mimetype %s\n' % mime)
    sys.exit(1)
  data = json.load(response)

  access = data.get('access', 'none')
  if access in ('r', 'rw'):
    hgcmd = ['-R', data['path'], 'serve', '--stdio']
    if access == 'r':
      hgcmd += [
        '--config',
        'hooks.prechangegroup.readonly=python:%s.permissiondenied' % __name__,
        '--config',
        'hooks.prepushkey.readonly=python:%s.permissiondenied' % __name__,
      ]
    mercurial.dispatch.dispatch(mercurial.dispatch.request(hgcmd))
  else:
    sys.stderr.write('Permission denied\n')
    sys.exit(1)

def permissiondenied(ui, **kwargs):
  ui.warn('Permission denied\n')
  return 1
