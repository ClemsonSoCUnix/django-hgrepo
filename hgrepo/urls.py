try:
  from django.conf.urls import patterns, url
except ImportError:
  from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('hgrepo.views',
  url(r'^access/(?P<repo>.+)$', 'access', name='access'),
)
